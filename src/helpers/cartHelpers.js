import { cartData } from '../stores/store.js';

const readCart = () => {
  let cart = localStorage.cart;
  if (cart) {
    cartData.set(JSON.parse(cart));
  }
  // запрос актуальных цен
}

const saveCart = cart => {
  localStorage.cart = JSON.stringify(cart);
}

const countSumAndSave = cart => {
  cart.sum = Object.entries(cart.list).reduce((acc, el) => acc + el[1].price * el[1].quantity, 0);
  cartData.set(cart);
  saveCart(cart);
}

const addToCart = (product, oldCart) => {
  if (!oldCart.list[product.id]) {
    oldCart.list[product.id] = {
      variant_id: product.id,
      quantity: 1,
      price: product.price * 1
    }
  } else {
    oldCart.list[product.id].quantity += 1;
  }
  countSumAndSave(oldCart);
}

const deleteOne = (product, oldCart) => {
  const cartProduct = oldCart.list[product.id];
  if (cartProduct) {
    cartProduct.quantity -= 1;
    if (cartProduct.quantity === 0) {
      deletePosition (product, oldCart);
      return;
    }
    countSumAndSave(oldCart);
  }
}

const clearCart = () => cartData.set({list : {}});

const deletePosition = (product, oldCart) => {
  if (oldCart.list[product.id]) {
    delete oldCart.list[product.id];
    countSumAndSave(oldCart);
  }
}

export default {
  addToCart,
  deleteOne,
  deletePosition,
  clearCart,
  readCart
};
