const productNumFormat = (n) => {
  const ones = n % 10;
  const tens = (n % 100 - ones) / 10;
  let str = ' товаров';
  
  if (!n && n !== 0) {
    return '';
  } else if (n === 0) {
    return 'Здесь пока пусто.';
  } else if (ones === 1 && tens !== 1) {
    str = ' товар';
  } else if (tens !== 1 && ones > 1 && ones < 5) {
    str = ' товара';
  }
  return n + str;
}

export default {
  productNumFormat
};
