const level2tax = (pref, links, names, id, pretty_name) => {
  if (links[2]) {
    return {
      link: links[2],
      name: names[2],
      permalink: `${pref}/${links[0]}/${links[1]}/${links[2]}`,
      pretty_name,
      id
    }
  }
}

const level1tax = (pref, links, names, id, pretty_name) => {
  let res = {
    link: links[1],
    name: names[1],
    permalink: `${pref}/${links[0]}/${links[1]}`,
    pretty_name : pretty_name.split(' -> ').slice(0,2).join(' -> ')
  }
  const taxon = level2tax(pref, links, names, id, pretty_name);
  if (taxon) {
    res.taxons = [taxon];
  } else {
    res.id = id;
    res.pretty_name = pretty_name;
  }
  return res
}

const level0tax = (pref, links, names, id, pretty_name) => ({
  link: links[0],
  name: names[0],
  permalink: `${pref}/${links[0]}`,
  pretty_name: pretty_name.split(' -> ').slice(0,1).join(''),
  taxons: [level1tax(pref, links, names, id, pretty_name)]
});

const arrayToTree = (taxons, pref) => {
  const res = [];
  taxons.filter(tax => tax.products_count > 0).forEach(taxon => {
    const names = taxon.pretty_name.split(' -> ');
    const links = taxon.permalink.split('/');
    const { id, pretty_name } = taxon;
    const level0 = res.filter(tax => tax.link === links[0]);

    if (level0.length === 0) {
      res.push(level0tax(pref, links, names, id, pretty_name));
    } else {
      const level1 = level0[0].taxons.filter(tax => tax.link === links[1]);
      if (level1.length === 0) {
        level0[0].taxons.push(level1tax(pref, links, names, id, pretty_name));
      } else {
        if (level1[0].taxons) {
          const level2 = level1[0].taxons.filter(tax => tax.link === links[2]);
          if (level2.length === 0) {
            level1[0].taxons.push(level2tax(pref, links, names, id, pretty_name));
          }
        }
      }
    }
  });
  return res;
};

const getCat = (curCat, path, counter) => {
  const { cat, subcat, subsubcat } = path;
  const pathArray = [cat, subcat, subsubcat];
  const c = counter || 0;
  if (curCat
    && curCat.taxons
    && curCat.taxons.length
    && curCat.taxons.length > 0
    && pathArray[c]
  ) {
    return getCat(curCat.taxons.filter(tax => tax.link === pathArray[c] || tax.permalink.match(new RegExp(`${pathArray[c]}$`)))[0], path, c + 1);
  }
  return curCat;
};

const childrenCats = (tree, path) => {
  const activeCategory = getCat(
    { taxons: tree },
    path,
  );
  const { taxons } = activeCategory;
  const childrenCats = (taxons && taxons.length && taxons.length > 0)
    ? taxons : null;
  return { childrenCats, activeCategory };
};

const formCrumbs = (path, room, dict, singleCat) => {
  let crumbs = [{
    name: 'Главная',
    permalink: '/'
  }];

  if (path) {
    const { cat, subcat, subsubcat } = path;
    let pref = '';

    if (room) {
      crumbs = crumbs.concat([
        {
          name: 'Комнаты',
          permalink: null
        }, {
          name: room.name,
          permalink: `/rooms/${room.key}`
        }
      ]);
      pref = `/rooms/${room.key}`;
    } else if (singleCat == '3014') {
      crumbs.push({
        name: 'Материалы',
        permalink: null
      });
      pref = `/materials`;
    } else if (cat) {
      crumbs.push({
        name: 'Каталог',
        permalink: null
      });
      pref = `/catalog`;
    } else {
      crumbs[0].permalink = null;
      return crumbs
    }

    // temp

    const p = [cat, subcat, subsubcat].map(c => ({link: c, name: dict[c]}))

    // temp /

    crumbs = crumbs.concat(p.reduce((acc, cat, i) => {
      if (cat.link) {
        if (i === 0) {
          cat.permalink = `${pref}/${cat.link}`;
        } else {
          const prev = acc[acc.length - 1];
          cat.permalink = `${prev.permalink}/${cat.link}`;
        }
        acc.push(cat);
      }
      return acc;
    }, []));
  }
  crumbs[crumbs.length - 1].permalink = null;
  return crumbs;
};

const formDict = (categories, dict) => {
  let d = dict || {};
  return categories.reduce ? categories.reduce((acc, cat) => {
    acc[cat.permalink.split('/').reverse()[0]] = cat.name;
    if (cat.taxons) {
      acc = { ...acc, ...formDict(cat.taxons)};
    }
    return acc;
  }, d) : d;
}

const formRequest = ({
  activeCategory,
  activeRoom,
  query,
  sorter,
  filterOpts,
  filterRanges
}) => {
  const requestParams = { ...query };
  let paramsString = '?';

  if (filterRanges) {
    Object.entries(filterRanges).forEach(limit => {
      paramsString += `${limit[0]}=${limit[1]}&`;
    });
  }

  if (filterOpts) {
    Object.entries(filterOpts).forEach(filter => {
      let props = Object.keys(filter[1]);
      if (props.length === 1) {
        paramsString += `q[${filter[0]}_matches]=${props[0]}&`;
      } else {
        props.forEach(prop =>
          paramsString += `q[${filter[0]}_matches_any][]=${prop}&`
        );
      }
    });
  }

  if (activeCategory) {
    requestParams['q[taxons_id_eq]'] = activeCategory.id;
  }

  if (activeRoom) {
    requestParams['q[feature_apartment_eq]'] = activeRoom.id;
  }

  if (sorter) {
    requestParams['q[s]'] = `rub_price_amount+${sorter}`;
  }

  paramsString = Object.keys(requestParams)
    .reduce((res, key) => `${res}${key}=${requestParams[key]}&`, paramsString);

  return paramsString.slice(0, -1);
};

const formTitle = (activeRoom, activeCategory) => {
  let room = activeRoom ? activeRoom.name : null;
  let cat = (activeCategory ? activeCategory.name : null) || null;
  if (!room && !cat) {return ''}
  if (!room || !cat) {
    return room ? room : cat;
  }
  return `${room}: ${cat}`
}

const prepareMenu = (categories) => {
  let cats = categories.taxonomies.map(t => t.root);
  cats.forEach((cat, i) => {
    cats[i].taxons = cat.taxons.filter(subcat => subcat.products_count > 0);
    cat.taxons.forEach((subcat, j) => {
      cats[i].taxons[j].taxons = cats[i].taxons[j].taxons.filter(subsubcat => subsubcat.products_count > 0);
    });
  });
  const dict = formDict(cats);
  let cat = cats.filter(cat => cat.id == '3014');
  cat = JSON.parse(JSON.stringify(cats.filter(cat => cat.id == '3014')[0]).replace(/chiernovyie-matierialy\//g, '')).taxons;
  cats = cats.filter(cat => cat.id != '3014');
  return [cats, cat, dict];
}

const getImgURL = (name) => {
  if (!name) {
    return 'no name';
  }

  let url = name.toLowerCase().split(' -> ');
  const last = url
    .slice(-1)[0]
    .replace(/ /g, '-')
    .replace(/\//g, '%2C-');

  url.pop();
  url.push(last);
  url = url.join('/').replace(/ /g, '+');
  return `https://rerooms-store.s3-us-west-2.amazonaws.com/categories/${url}.jpg`;
}

const getPref = (room, singleCat) => {
  let pref = room ? '' : 'catalog/';
  return (singleCat == '3014') ? 'materials/' : pref;
}

export default {
  getCat,
  formCrumbs,
  arrayToTree,
  childrenCats,
  formRequest,
  formDict,
  formTitle,
  prepareMenu,
  getImgURL,
  getPref
};
