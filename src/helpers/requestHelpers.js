import dataHelpers from '../helpers/dataHelpers';
import * as api from 'api.js';
import {
  pathData,
  childrenCatsData,
  activeCategoryData,
  activeRoomData,
  productsData,
  filtersData,
  queryData,
  isLowestData,
  sorterData,
  filterOptsData,
  filterRangesData,
  roomMenuData,
  singleCatData
} from '../stores/store.js';

function resetCategoryInner() {
  productsData.set(null);
  filtersData.set(null);
  filterOptsData.set({});
  filterRangesData.set({});
  sorterData.set('asc');
  queryData.set({});
}

async function updateCatalog(path, query, oldCategory, oldQuery, categories, sorter, filterOpts, filterRanges, activeRoom) {
  const { childrenCats, activeCategory } = dataHelpers.childrenCats(categories, path);

  if (childrenCats) {
    resetCategoryInner();
    queryData.set({});
  } else {
    queryData.set(query);
    if ((oldCategory !== activeCategory)
      || (oldQuery.page !== query.page)) {
      let products;
      const doNotReloadFilters = oldCategory === activeCategory;
      if (doNotReloadFilters) {
        productsData.set(null);
        products = await api.getProducts(dataHelpers.formRequest({
          activeCategory,
          activeRoom,
          query,
          sorter,
          filterOpts,
          filterRanges,
        }));
      } else {
        resetCategoryInner();
        const lowestContent = await api.getProdsAndFilts(dataHelpers.formRequest({
          activeCategory,
          activeRoom,
          query,
          sorter,
          filterOpts : {},
          filterRanges : {}
        }));
        products = lowestContent[0];
        let filters = lowestContent[1];
        filters = (filters && filters.main && filters.additional)
          ? filters.main.concat(filters.additional) : null;
        filtersData.set(filters);
      }
      productsData.set(products);
    }
  }
  isLowestData.set(!childrenCats);
  activeCategoryData.set(activeCategory);
  childrenCatsData.set(childrenCats);
  pathData.set(path);
}

export default {
  updateCatalog,
  resetCategoryInner
};
